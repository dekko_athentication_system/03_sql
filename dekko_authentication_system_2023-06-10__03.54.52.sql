--
-- PostgreSQL database dump
--

-- Dumped from database version 14.8 (Ubuntu 14.8-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.8 (Ubuntu 14.8-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dekko; Type: SCHEMA; Schema: -; Owner: dekko
--

CREATE SCHEMA dekko;


ALTER SCHEMA dekko OWNER TO dekko;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: dekko; Owner: dekko
--

CREATE TABLE dekko.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE dekko.flyway_schema_history OWNER TO dekko;

--
-- Name: login; Type: TABLE; Schema: dekko; Owner: dekko
--

CREATE TABLE dekko.login (
    oid character varying(128) NOT NULL,
    user_id character varying(128),
    password character varying(256) NOT NULL,
    role_oid character varying(128) NOT NULL,
    status character varying(32) NOT NULL,
    is_logged_in character varying(32) DEFAULT 'No'::character varying NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_is_logged_in_login CHECK ((((is_logged_in)::text = 'Yes'::text) OR ((is_logged_in)::text = 'No'::text))),
    CONSTRAINT ck_status_login CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE dekko.login OWNER TO dekko;

--
-- Name: registration; Type: TABLE; Schema: dekko; Owner: dekko
--

CREATE TABLE dekko.registration (
    oid character varying(128) NOT NULL,
    registration_id character varying(128) DEFAULT 'System'::character varying NOT NULL,
    first_name character varying(128) DEFAULT 'current_timestamp'::character varying NOT NULL,
    last_name character varying(128) NOT NULL,
    email character varying(256) NOT NULL,
    mobile character varying(128) NOT NULL,
    address text,
    date_of_birth character varying NOT NULL,
    status character varying(32) NOT NULL,
    login_oid character varying,
    created_by character varying(128) DEFAULT 'System'::character varying,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_registration CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE dekko.registration OWNER TO dekko;

--
-- Name: role; Type: TABLE; Schema: dekko; Owner: dekko
--

CREATE TABLE dekko.role (
    oid character varying(128) NOT NULL,
    role_id character varying(32),
    role_name_en character varying(128) NOT NULL,
    role_name_bn character varying(128),
    role_description character varying(128) NOT NULL,
    menu_json character varying(256) NOT NULL,
    status character varying(128) NOT NULL,
    created_by character varying DEFAULT 'System'::character varying,
    created_on character varying(32) DEFAULT 'current_timestamp'::character varying,
    updated_by character varying(128),
    updated_on character varying(128)
);


ALTER TABLE dekko.role OWNER TO dekko;

--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: dekko; Owner: dekko
--

COPY dekko.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
0	\N	<< Flyway Schema Creation >>	SCHEMA	"dekko"	\N	dekko	2023-06-10 03:49:42.801521	0	t
1	\N	01 00 00  security	SQL	00-common/R__01_00_00__security.sql	-873934969	dekko	2023-06-10 03:49:42.830155	42	t
2	\N	07 00 00  security	SQL	00-common/R__07_00_00__security.sql	1329590822	dekko	2023-06-10 03:49:42.90116	12	t
\.


--
-- Data for Name: login; Type: TABLE DATA; Schema: dekko; Owner: dekko
--

COPY dekko.login (oid, user_id, password, role_oid, status, is_logged_in, created_by, created_on, updated_by, updated_on) FROM stdin;
LGN-OID-ADMIN	admin	admin	Role-Oid-001	Active	No	System	2023-06-10 03:49:42.894852	\N	\N
LGN-0000001	ishtiak	123456	Role-Oid-002	Active	No	System	2023-06-10 03:49:42.894852	\N	\N
\.


--
-- Data for Name: registration; Type: TABLE DATA; Schema: dekko; Owner: dekko
--

COPY dekko.registration (oid, registration_id, first_name, last_name, email, mobile, address, date_of_birth, status, login_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
REGISTRATION-OID-0000001	REG-0000001	Admin	Admin	admin@gmail.com	01915564675	Motijheel	1988-06-11	Active	LGN-OID-ADMIN	System	2023-06-10 03:49:42.898229	\N	\N
REGISTRATION-OID-0000002	REG-0000002	Ishtiak	Ahmed	ishtiak@gmail.com	01712621310	Mirpur	1988-03-16	Active	LGN-0000001	System	2023-06-10 03:49:42.898229	\N	\N
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: dekko; Owner: dekko
--

COPY dekko.role (oid, role_id, role_name_en, role_name_bn, role_description, menu_json, status, created_by, created_on, updated_by, updated_on) FROM stdin;
Role-Oid-001	ADMIN	Admin	অ্যাডমিন	Admin	[{"text":"Dashboard","icon":"dashboard","url":"/dashboard"},{"text":"Registration","icon":"receipt","children":[{"text":"Registration List","icon":"local_atm","url":"/registration"},{"text":"Add Registration","icon":"history","url":"/registration/add"}]}]	Active	System	current_timestamp	\N	\N
Role-Oid-002	USER	User	ব্যবহারকারী	User	[{"text":"Dashboard","icon":"dashboard","url":"/dashboard"}]	Active	System	current_timestamp	\N	\N
\.


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: login pk_login; Type: CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.login
    ADD CONSTRAINT pk_login PRIMARY KEY (oid);


--
-- Name: registration pk_registration; Type: CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.registration
    ADD CONSTRAINT pk_registration PRIMARY KEY (oid);


--
-- Name: role pk_role; Type: CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.role
    ADD CONSTRAINT pk_role PRIMARY KEY (oid);


--
-- Name: login uk_user_id_login; Type: CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.login
    ADD CONSTRAINT uk_user_id_login UNIQUE (user_id);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: dekko; Owner: dekko
--

CREATE INDEX flyway_schema_history_s_idx ON dekko.flyway_schema_history USING btree (success);


--
-- Name: registration fk_login_oid_registration; Type: FK CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.registration
    ADD CONSTRAINT fk_login_oid_registration FOREIGN KEY (login_oid) REFERENCES dekko.login(oid);


--
-- Name: login fk_role_oid_login; Type: FK CONSTRAINT; Schema: dekko; Owner: dekko
--

ALTER TABLE ONLY dekko.login
    ADD CONSTRAINT fk_role_oid_login FOREIGN KEY (role_oid) REFERENCES dekko.role(oid);


--
-- PostgreSQL database dump complete
--

